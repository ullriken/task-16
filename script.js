// Bank Balance
var balance = 0.14;
const loan = 5900;
var loanTries = 0;

BankBalance();
function BankBalance(){
document.getElementById("bankBalance").innerHTML = ("Bank balance: "+ balance + " kr");
}

// Work For Money
document.getElementById("btn-workForMoney").addEventListener("click", function(){
    console.log("Work")
    balance =parseInt(balance + 10);
    BankBalance();
  });

// Get Loan from Bank
document.getElementById("btn-getLoanFromBank").addEventListener("click", function(){
    console.log("Loan");
    if (loanTries < 1) {
        loanTries = loanTries + 1;
        balance =parseInt(balance + loan);
        document.getElementById("btn-getLoanFromBank").disabled = true;
    }
    BankBalance();
  });

// Computer Option Value
var userSelected = document.getElementById("sComputer");

// Buy Computer
document.getElementById("btn-buyComputer").addEventListener("click", function(){
    console.log("Buy")

    // Getting price if Computer
    let computerPrice =parseInt(userSelected.value);
    console.log(computerPrice);

    // Checking Balance vs Computer Price
    if (computerPrice <= balance){
        balance =parseInt(balance - computerPrice);
        BankBalance();
        let computerSelected = userSelected.options[userSelected.selectedIndex];
        alert("Congratulation! You are now a proud owner of " + computerSelected.id);
    }

    // BANNED message and lock functions
    else {
        //TODO
        document.getElementById("btn-buyComputer").disabled = true;
        document.getElementById("btn-getLoanFromBank").disabled = true;
        document.getElementById("sComputer").disabled = true;
        alert("You have been BANNED !!!");
    }
  });

